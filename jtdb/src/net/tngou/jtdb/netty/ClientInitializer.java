package net.tngou.jtdb.netty;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;


/**

 * Netty Client 客户端设置
 * @author tngou@tngou.net
 *
 */
public class ClientInitializer extends ChannelInitializer<SocketChannel> {
private static final ClientHandler ClIENT_HANDLER = new ClientHandler();


@Override
public void initChannel(SocketChannel ch) throws Exception {
	ChannelPipeline pipeline = ch.pipeline();
	pipeline.addLast(new ObjectEncoder());
	pipeline.addLast( new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
	pipeline.addLast(ClIENT_HANDLER);
	
	}
}

  
