package net.tngou.jtdb.sql;

import net.tngou.jtdb.netty.TngouClient;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * 连接池创建
 * @author tngou@tngou.net
 *
 */
public class DataSourcePoolableObjectFactory  extends BasePooledObjectFactory<TngouClient>{

	

	@Override
	public PooledObject<TngouClient> wrap(TngouClient tngouClient) {
		return new DefaultPooledObject<TngouClient>(tngouClient);
	}

	@Override
	public TngouClient create() throws Exception {
		return new TngouClient();
	}

	

}
