package net.tngou.jtdb.sql;


import java.sql.SQLException;
import net.tngou.jtdb.netty.TngouClient;


/**
 * TnDB数据操作
 * @author tngou@tngou.net
 *
 */
public class Connection {
	
	private static TngouClient tngouClient =null;
	/**
	 *  取得数据库链接
	 * @return 数据库链接
	 * @throws SQLException
	 */
	public static Connection getConnection() 
	{
		if(tngouClient!=null) return new Connection();
		tngouClient=DBManager.getConnection();
		return new Connection();
		
	}
	
	/**
	 * 链接回收
	 */
	public static void closeConnection() 
	{
		if(tngouClient!=null)DBManager.closeConnection(tngouClient); 
		tngouClient=null;
	}
	
	
	/**
	 * 
	* @Title: close
	* @Description: 关闭连接
	* @param     设定文件
	* @return void    返回类型
	* @throws
	 */
	public static void close() 
	{
		  if(tngouClient==null){
			  tngouClient=DBManager.getConnection(); 
		  }
		  DBManager.close(tngouClient);
		  tngouClient=null;
	}
	

	/**
	 * 创建连接
	 * @return
	 */
	public  Statement createStatement() 
	{
			if(tngouClient==null)tngouClient=DBManager.getConnection();
			return new Statement(tngouClient);
	}
	
}
