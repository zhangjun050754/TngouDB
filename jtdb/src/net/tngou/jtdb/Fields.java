package net.tngou.jtdb;

import java.util.ArrayList;
import java.util.List;

/**
 * 
* @Description: 数据列
* @author tngou@tngou.net 
* @date 2015年5月21日 下午3:41:38
*
 */
public class Fields {

   private	List<Field> list = new ArrayList<Field>();
	public void add(Field field) {
		list.add(field);
	}
	public List<Field> getList() {
		return list;
	}
	public void setList(List<Field> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		String s="";
		for (Field e : list) {
			s=s+"name:"+e.getName()+" "+"value:"+e.getValue()+" "+"type:"+e.getType()+"\n";
		}
		return s;
	}
	

}
