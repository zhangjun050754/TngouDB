package net.tngou.jtdb;


import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import net.tngou.db.util.ResultSet;
import net.tngou.jtdb.sql.Connection;
import net.tngou.jtdb.sql.Statement;


/**
 * 
* @Description: jtdb主要的工具类，用于直接操作TngouDB
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年6月17日 下午2:21:42
*
 */
public class TngouDBHelp {

	
	
	
	private static Connection con = null;
	private static TngouDBHelp tngouDBHelp=null;
	
	/**
	 * 
	* @Description: 实现单链创建
	* @param @return    设定文件
	* @return TngouDBHelp    返回类型
	* @throws
	 */
	public static TngouDBHelp getConnection()  {
		if(tngouDBHelp!=null) return tngouDBHelp;
		return new TngouDBHelp();
		
	}
	
	private TngouDBHelp() {
		con=Connection.getConnection();
	}
	
	
	/**
	 * 
	* @Description:  创建表
	* @param  tableName  表名
	 */
	public ResultSet createTable(String tableName) {
		Statement stmt = con.createStatement();
		String sql ="create table "+tableName;
		return stmt.execute(sql);			
	}
	
	
	/**
	 * 
	* @Description: 删除表
	* @param tableName    表名
	 */
	public ResultSet dropTable(String tableName) {
		Statement stmt = con.createStatement();
		String sql ="drop table "+tableName;		
		return stmt.execute(sql);	
	}
	
	
	/**
	 * 
	* @Description: 插入数据
	* @param  tableName  插入数据表
	* @param  fields    值
	* @throws
	 */
	public ResultSet insert(String tableName ,Field ...fields) {
	
		Statement stmt = con.createStatement();
		String[] params =new String[fields.length];
		String fs="";
		String values="";
		for (int i = 0; i < fields.length; i++) {
			if(i!=0){fs+=",";values+=",";}
			fs+=fields[i].getName()+":"+fields[i].getType();
			values+="?";
			params[i]=fields[i].getValue();
		}
		
		String sql ="insert into "+tableName
				  +"("+fs+") "
				  + " values("+values+")";
	
		
		return stmt.execute(sql,params);
	}
	
	
	/**
	 * 
	* @Description: 插入数据
	* @param @param tableName
	* @param @param fields    设定文件
	* @return void    返回类型
	* @throws
	 */
	public ResultSet insert(String tableName ,Fields fields) {
	
		List<Field> list = fields.getList();
		Field[] fs = (Field[]) list.toArray(new Field[list.size()] ); 
		return insert( tableName ,fs);
		
	}
	
	
	/**
	 * 
	* @Description: 查询数据
	* @param @param tableName  表名
	* @param @param sortField  排序
	* @param @param page  当前页
	* @param @param size  大小
	* @param @param fields 查询条件
	* @return Page    返回类型
	* @throws
	 */
	public Page select(String tableName ,SortField sortField,int page,int size,Field ...fields) {
		
		 Page p = new Page();
		 if(page < 0 || size < 0){page=1;size=1;}
		 int from = (page - 1) * size;
		 size = (size > 0) ? size : Integer.MAX_VALUE;
		 Statement stmt = con.createStatement();
		 String sql="select * from " +tableName;
		 String[] params = null; 
		 if(fields.length>0)
		 {
			 params= new String[fields.length];
			 sql=sql+" where ";
			for (int i = 0; i < fields.length; i++) {
				if(i!=0)  sql=sql+" and ";
				sql=sql+fields[i].getName()+":"+fields[i].getType()+"=?";
				params[i]=fields[i].getValue();
			}
		 }
		 
		 if(sortField!=null)
			 sql=sql+ " order by "+sortField.getName()+":"+sortField.getType() +" "+ sortField.getOrder();
		 sql=sql+ " limit "+from+","+size;
		
		 ResultSet resultSet=null;
		 try {
				if(params==null)params= new String[0];
				
			     resultSet=stmt.execute(sql,params);
				BeanUtils.copyProperties(p, resultSet);
			} catch ( IllegalAccessException | InvocationTargetException e) {
				
				p.setStatus(ResultSet.T_SUCCESS_NOT_FULL);//返回203
			}
		return p;
		
	}
	
	/**
	 * 
	* @Description: 执行SQL语句，通用模式
	* @param  sql  SQL语句
	* @param params  可变参数
	* @throws
	 */
	public ResultSet execute(String sql,String ... params) {
		Statement stmt = con.createStatement();	
		return stmt.execute(sql, params);
		
	}
	
	
	/**
	 *  
	* @Description: 回收连接池
	 */
	public void closeConnection() {
		Connection.closeConnection(); // 回收链接
	}
	
	
	/**
	 * 
	* @Description: 关闭链接
	 */
	public void close() {
		Connection.close();            // 关闭链接
	}
	
	
	
	

  
}
