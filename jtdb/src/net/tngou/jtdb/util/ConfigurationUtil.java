package net.tngou.jtdb.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





/**
 * 配置文件加载
 * @author tngou@tngou.net
 *
 */
public class ConfigurationUtil {
	private static Logger log = LoggerFactory.getLogger(ConfigurationUtil.class);
	private static  ConfigurationUtil CONFIG= null;
	public final static ConfigurationUtil getInstance (){
		if(CONFIG!=null) return CONFIG; //判断是否加载		
		ConfigurationUtil conf = new ConfigurationUtil();
		Configurations configs = new Configurations();
		File propertiesFile = new File("tngou.properties");
		try {
			PropertiesConfiguration config = configs.properties(propertiesFile);
			Map<String,  Object> cp_props = new HashMap<String, Object>();
			
			Iterator<String> iterable = config.getKeys();
			while (iterable.hasNext()) 
			{
				String skey=iterable.next();
				cp_props.put(skey, config.getProperty(skey));
			}
			BeanUtils.populate(conf, cp_props);
		} catch (ConfigurationException | IllegalAccessException | InvocationTargetException e) {
			log.error("找不到配置文件{}" ,propertiesFile.getPath());
			return new ConfigurationUtil();
		}
		return conf;	
	}
	
	/**
	 * 参数设置
	 */
	private String host;//链接地址
	private int port;//端口
	private int overtime=6000;//链接超时
	private int maxidle=5;//最大空闲数。
	private int minidle=2;//最小空闲数
	private int maxtotal=10;//最大连接池
	private long maxwaitmillis=6000;//最大等待时间
	public static Logger getLog() {
		return log;
	}
	public static void setLog(Logger log) {
		ConfigurationUtil.log = log;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getOvertime() {
		return overtime;
	}
	public void setOvertime(int overtime) {
		this.overtime = overtime;
	}
	public int getMaxidle() {
		return maxidle;
	}
	public void setMaxidle(int maxidle) {
		this.maxidle = maxidle;
	}
	public int getMinidle() {
		return minidle;
	}
	public void setMinidle(int minidle) {
		this.minidle = minidle;
	}
	public int getMaxtotal() {
		return maxtotal;
	}
	public void setMaxtotal(int maxtotal) {
		this.maxtotal = maxtotal;
	}
	public long getMaxwaitmillis() {
		return maxwaitmillis;
	}
	public void setMaxwaitmillis(long maxwaitmillis) {
		this.maxwaitmillis = maxwaitmillis;
	}
	
	
}
