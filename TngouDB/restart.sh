#!/bin/bash
echo "restart TngouDB......"
CMD_PATH=`dirname $0`
echo $CMD_PATH
RUN_PATH=$CMD_PATH"/bin"
LIB_PATH=$CMD_PATH"/lib"
echo $RUN_PATH
echo $LIB_PATH
##JAVA_OPTS=-server -Xms512m -Xmx512m
java -Djava.ext.dirs=$LIB_PATH $JAVA_OPTS -cp $RUN_PATH net.tngou.db.netty.RestartServer &
sleep 6
echo "TngouDB run"
exit;
