package net.tngou.db.test;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.Configuration;
import javax.cache.configuration.MutableConfiguration;


public class JSR107 {

	
	public static void main(String[] args) {
		
		 
		//初始化 CacheManager
		CacheManager cacheManager = Caching.getCachingProvider().getCacheManager();		
		
		//创建Cache
		Configuration<Object, Object> configuration = new MutableConfiguration<Object, Object>();
		cacheManager.createCache("tngou", configuration);
		//取得Cache
		Cache<Object, Object> tngou_put = cacheManager.getCache("tngou");
		
		//Cache 操作
		tngou_put.put("chen", "lei");		
//		cacheManager.close();		
		Cache<Object, Object> tngou_get = cacheManager.getCache("tngou");
		System.err.println(tngou_get.get("chen"));
		
	}
	
	
	
	/**
	 * 取得缓存CacheManager
	 */
	public CacheManager getCacheManager() {
		
		return Caching.getCachingProvider().getCacheManager();
	}
	
	
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	public void createCacheName(String name) {	  
		    getCacheManager().createCache(name, new MutableConfiguration());
    
	}
}
