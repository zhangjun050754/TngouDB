package net.tngou.db.netty.http;

import static io.netty.handler.codec.http.HttpResponseStatus.CONTINUE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.MemoryAttribute;
import io.netty.handler.codec.http.multipart.InterfaceHttpData.HttpDataType;
import io.netty.util.AsciiString;

public class TngouHttpUtil {
	    private static final AsciiString CONTENT_TYPE = new AsciiString("Content-Type");
	    private static final AsciiString CONTENT_LENGTH = new AsciiString("Content-Length");
	    private static final AsciiString CONNECTION = new AsciiString("Connection");
	    private static final AsciiString KEEP_ALIVE = new AsciiString("keep-alive");
		private static final String CHARTSET = "UTF-8";
	
	
	
	public static void run(ChannelHandlerContext ctx,byte[] bytes)
	{
		
	
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(bytes));
        response.headers().set(CONTENT_TYPE, "application/json;charset=utf-8");
        response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes());

        response.headers().set(CONNECTION, KEEP_ALIVE);
        ctx.write(response);
        
	}
	public static void run(ChannelHandlerContext ctx,HttpRequest req,byte[] bytes)
	{
		
		if (HttpUtil.is100ContinueExpected(req)) {
            ctx.write(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
        }    
        boolean keepAlive = HttpUtil.isKeepAlive(req);
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(bytes));
        response.headers().set(CONTENT_TYPE, ContentType.getType(req.uri()));
        response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes());

        if (!keepAlive) {
            ctx.write(response).addListener(ChannelFutureListener.CLOSE);
        } else {
            response.headers().set(CONNECTION, KEEP_ALIVE);
            ctx.write(response);
        }
	}
	public static void run_404(ChannelHandlerContext ctx)
	{
		 StringBuffer buffer = new StringBuffer("TngouDB提示-没有找到请求资源：");
		 byte[] bytes={' '};
		 try {
			bytes = buffer.toString().getBytes(CHARTSET);
		 } catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		 }
		 FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer(bytes));
         response.headers().set(CONTENT_TYPE, "text/html;charset=utf-8");
         response.headers().setInt(CONTENT_LENGTH, response.content().readableBytes());
         response.headers().set(CONNECTION, KEEP_ALIVE);
         ctx.write(response);
         
	}
}
