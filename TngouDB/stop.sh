echo "stop TngouDB......"
CMD_PATH=`dirname $0`
echo $CMD_PATH
RUN_PATH=$CMD_PATH"/bin"
LIB_PATH=$CMD_PATH"/lib"
echo $RUN_PATH
echo $LIB_PATH
java -Djava.ext.dirs=$LIB_PATH $JAVA_OPTS -cp $RUN_PATH net.tngou.db.netty.StopServer
sleep 6
echo "TngouDB stop"
exit;
